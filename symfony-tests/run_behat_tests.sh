#!/usr/bin/env bash

set -e

function finish {
    echo "Cleaning..."
    # Cleanup commands here
    php app/console server:stop 0.0.0.0:8001
    # Stop Selenium Server that runs in background
    kill $!
}
trap finish EXIT


java -jar -Djava.util.logging.config.file=./tools/selenium-logging.properties ./tools/selenium-server-standalone-2.48.2.jar -p 4444 &
# Or following for Chrome (requires Chrome driver to be downloaded):
# java -jar -Djava.util.logging.config.file=./tools/selenium-logging.properties ./tools/selenium-server-standalone-2.48.2.jar -p 4444 -Dwebdriver.chrome.driver="/path/to/chromedriver" &

php app/console server:start --env=behat --router="vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/config/router_dev.php" 0.0.0.0:8001

php app/console doctrine:schema:update --force --env=behat

./bin/behat --config app/config/behat.yml

echo "Tests execution finished"