symfony-tests
=============

A Symfony project that represents different kinds of testing (unit, functional, acceptance).

#Running project
Run *composer install* command.  
Update configuration inside *app/config/parameters.yml* file  

Start Symfony server:
<code>php app/console server:start</code>

#Runnning tests
Run *composer install* command.  

##Configure PhpUnit tests to be runnable inside PhpStorm IDE
PhpStorm 8 requires PhpUnit 4.5, not newer.  
Inside *Configuration(Preferences) -> Languages & Frameworks -> PHP -> PHPUnit*  
select *Use custom autoloader*  
and fill paths for *Path to script*  (vendor/autoload.php)
and for *Default configuration file*  (app/phpunit.xml)  

Run test or project as *PHPUnit*
in Run/Debug configuration

##Running PHPUnit tests
Update configuration for PHPUnit test inside *app/config/parameters.yml*  
  
Create database for tests with the name provided inside parameters.yml file for parameter *database_test_name*   
 
Run tests with command:   
<code>./bin/phpunit -c app/</code>

##Running Behat tests
Update configuration for PHPUnit test inside *app/config/parameters.yml*  
  
Create database for tests with the name provided inside parameters.yml file for parameter *database_behat_name*  

Simply run the following command:  
<code>./run_behat_tests.sh</code>  

