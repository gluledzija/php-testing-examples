<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\EmailService;

class EmailServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @dataProvider validationEmailProvider
     */
    public function testShouldValidateEmails($email, $shouldBeValid) {
        // Prepare
        $emailService = new EmailService();

        // Execute and assert
        $this->assertEquals($shouldBeValid, $emailService->isEmailValid($email));
    }

    public function validationEmailProvider()
    {
        return array(
            // Valid
            array('test@example.com', true),
            array('test+filter@example.com', true),
            // Invalid
            array('test@example', false),
            array('test.example.com', false),
        );
    }

    /**
     * @dataProvider sortingEmailProvider
     */
    public function testShouldSortEmails($inputEmails, $expectedEmails) {
        // Prepare
        $emailService = new EmailService();

        // Execute and assert
        $this->assertEquals($expectedEmails, $emailService->sortEmails($inputEmails));
    }

    public function sortingEmailProvider()
    {
        return array(
            // Regular emails sorting
            array(array('aaa@aaa.aaa', 'ccc@ccc.ccc', 'bbb@bbb.bbb'), array('aaa@aaa.aaa', 'bbb@bbb.bbb', 'ccc@ccc.ccc')),
            array(array('aaa@aaa.aaa', '111@111.111', 'bbb@bbb.bbb'), array('111@111.111', 'aaa@aaa.aaa', 'bbb@bbb.bbb')),
            // Sorting by domain
            array(array('aaa@aaa.aaa', 'bbb@ccc.ccc', 'ccc@bbb.bbb'), array('aaa@aaa.aaa', 'ccc@bbb.bbb', 'bbb@ccc.ccc')),
            array(array('111@aaa.aaa', 'aaa@111.111', 'bbb@bbb.bbb'), array('aaa@111.111', '111@aaa.aaa', 'bbb@bbb.bbb')),
            // Sorting primarily by domain then by local part
            array(array('333@aaa.aaa', '111@bbb.bbb', '222@aaa.aaa'), array('222@aaa.aaa', '333@aaa.aaa', '111@bbb.bbb')),
            array(array('111@bbb.bbb', '333@aaa.aaa', '222@aaa.aaa'), array('222@aaa.aaa', '333@aaa.aaa', '111@bbb.bbb')),
        );
    }
}
