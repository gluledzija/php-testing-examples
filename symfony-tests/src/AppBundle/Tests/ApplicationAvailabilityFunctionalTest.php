<?php

namespace AppBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ApplicationAvailabilityFunctionalTest extends WebTestCase {

    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();

        // Execute
        $client->request('GET', $url);

        // Assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function urlProvider()
    {
        return array(
            array('/'),
            array('/subscribers/1'),
        );
    }
}