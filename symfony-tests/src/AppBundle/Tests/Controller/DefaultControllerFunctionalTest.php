<?php

namespace AppBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class DefaultControllerFunctionalTest extends WebTestCase
{
    const FIRST_SUBSCRIBER_LINK = '#subscriber-link-1';

    public function testIndex()
    {
        // Prepare
        $client = static::makeClient();

        // Execute
        $crawler = $client->request('GET', '/');

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertContains('Welcome to test examples for Symfony', $crawler->filter('#container h1')->text());
    }

    public function testSubscriberSelectionFlow()
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();

        // Execute
        $crawler = $client->request('GET', '/');

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertEquals(1, $crawler->filter(self::FIRST_SUBSCRIBER_LINK)->count(), "Home page should display a subscriber link");
        $subscriberLink = $crawler->filter(self::FIRST_SUBSCRIBER_LINK)->eq(0)->link();
        $this->assertEquals('TestName', $subscriberLink->getNode()->textContent);

        // Execute
        $crawler = $client->click($subscriberLink);

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertEquals('Name: TestName', $crawler->filter('ul > li:nth-child(1)')->text());
        $this->assertEquals('Email: test@example.com', $crawler->filter('ul > li:nth-child(2)')->text());

        // Execute
        $crawler = $client->click($crawler->filter('a')->eq(0)->link());

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertEquals(1, $crawler->filter(self::FIRST_SUBSCRIBER_LINK)->count());
    }
}
