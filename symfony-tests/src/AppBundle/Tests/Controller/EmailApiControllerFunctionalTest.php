<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmailApiControllerFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider validEmailsProvider
     */
    public function testShouldProperlyValidate($email, $shouldBeValid)
    {
        // Prepare
        $client = static::createClient();
        $expectedResponseCode = $shouldBeValid ? 200 : 400;

        // Execute
        $crawler = $client->request('GET', '/api/emails/'.urlencode($email));

        // Assert
        $this->assertEquals($expectedResponseCode, $client->getResponse()->getStatusCode());
    }

    public function validEmailsProvider()
    {
        return array(
            // Valid
            array('test@example.com', true),
            array('test+filter@example.com', true),
            // Invalid
            array('test@example', false),
            array('test.example.com', false),
        );
    }

    /**
     * @dataProvider sortedLastEmailProvider
     */
    public function testShouldReturnSortedLastEmail($emails, $resultEmail)
    {
        // Prepare
        $client = static::createClient();

        // Execute
        $crawler = $client->request('POST', '/api/emails/action/sorted-last', $emails, array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            ),
            json_encode($emails));
        $response = json_decode($client->getResponse()->getContent());

        // Assert
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($resultEmail, $response->result, 'Last email after sorted email list should be returned');
    }

    public function sortedLastEmailProvider()
    {
        return array(
            array(array('aaa@aaa.aaa', 'ccc@ccc.ccc', 'bbb@bbb.bbb'), 'ccc@ccc.ccc'),
            array(array('111@111.111', '000@000.000', '222@222.222'), '222@222.222'),
        );
    }
}
