<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Service\ConverterService;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class SubscriberApiControllerTest extends WebTestCase
{
    public function testShouldReturnSubscriber()
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();
        $expectedResponse = json_encode(array(
            'id' => '1',
            'name' => 'TestName',
            'email' => 'test@example.com',
            'ip_address' => '111.222.333.444',
        ));

        // Execute
        $crawler = $client->request('GET', '/api/subscribers/1');

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertEquals(json_decode($expectedResponse),
            json_decode($client->getResponse()->getContent()));
    }

    public function testShouldNotReturnSubscriber()
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();

        // Execute
        $crawler = $client->request('GET', '/api/subscribers/2');

        // Assert
        $this->assertStatusCode(404, $client);
    }

    public function testShouldCreateSubscriber()
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();
        $expectedResponse = json_encode(array(
            'id' => 2,
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'ip_address' => '111.222.333.444', // Mock service object should return this
        ));
        $postingSubscriber = array(
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'domain' => 'www.google.com',
        );
        $this->mockConverterServiceForwardDnsLookup($client, '111.222.333.444');

        // Execute
        $crawler = $client->request('POST', '/api/subscribers', $postingSubscriber, array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            ),
            json_encode($postingSubscriber)
        );

        // Assert
        $this->assertStatusCode(200, $client);
        $this->assertEquals(json_decode($expectedResponse),
            json_decode($client->getResponse()->getContent()));
    }

    public function testShouldNotCreateSubscriber()
    {
        // Prepare
        $this->loadFixtures(array(
            'AppBundle\DataFixtures\ORM\TestFixture'
        ));
        $client = static::makeClient();
        $postingSubscriber = array(
            'name' => 'John Doe',
            'email' => 'test@example.com', // Email already exists
            'domain' => 'www.google.com',
        );
        $this->mockConverterServiceForwardDnsLookup($client, '111.222.333.444');

        // Execute
        $crawler = $client->request('POST', '/api/subscribers', $postingSubscriber, array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'ACCEPT' => 'application/json'
            ),
            json_encode($postingSubscriber)
        );

        // Assert
        $this->assertStatusCode(409, $client);
    }

    private function mockConverterServiceForwardDnsLookup($client, $returnString) {
        $serviceA = $this->getMockBuilder(ConverterService::class)->disableOriginalConstructor()->getMock();
        $serviceA->expects($this->any())->method('forwardDnsLookup')->will($this->returnValue($returnString));
        $client->getContainer()->set('converter_service', $serviceA);
    }
}
