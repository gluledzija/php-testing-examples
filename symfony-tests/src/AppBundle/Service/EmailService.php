<?php

namespace AppBundle\Service;

class EmailService
{

    const EMAIL_REGEX = "/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+/";

    public function isEmailValid($email)
    {
        return (bool)preg_match(self::EMAIL_REGEX, $email);
    }

    /**
     * Returns sorted emails primarily sorted by domain name, then sorted by local part
     * @param array $emails
     * @return array
     */
    public function sortEmails(array $emails)
    {
        usort($emails, function ($a, $b) {
            $domainA = $this->getDomainFromEmail($a);
            $domainB = $this->getDomainFromEmail($b);
            if ($domainA == $domainB) {
                return $this->cmpStrings($a, $b);
            }
            return ($domainA < $domainB) ? -1 : 1;
        });

        return $emails;
    }

    private function cmpStrings($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }

    function getDomainFromEmail($email)
    {
        // Get the data after the @ sign
        $domain = substr(strrchr($email, "@"), 1);

        return $domain;
    }
}