<?php

namespace AppBundle\Service;

use RestClient;

class ConverterService {

    public function forwardDnsLookup($domain) {
        $api = new RestClient(array(
            'base_url' => "http://api.konvert.me",
        ));
        $result = $api->get("forward-dns/".$domain, array());
        if($result->info->http_code == 200) {
            return $result->response;
        } else {
            return null;
        }
    }
}