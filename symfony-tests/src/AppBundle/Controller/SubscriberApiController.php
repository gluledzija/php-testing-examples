<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subscriber;
use AppBundle\Service\ConverterService;
use AppBundle\Service\EmailService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

class SubscriberApiController extends FOSRestController
{
    /**
     * @Get("/api/subscribers/{id}")
     * @param Request $request
     * @return View
     */
    public function getSubscriberAction(Request $request, $id)
    {
        $subscriberRepo = $this->getDoctrine()->getRepository('AppBundle:Subscriber');
        $subscriber = $subscriberRepo->find($id);

        if ($subscriber) {
            return $this->view($subscriber, 200);
        } else {
            return $this->view(
                array("message" => "There is no subscriber with requested ID"),
                404
            );
        }
    }

    /**
     * @Post("/api/subscribers")
     * @param Request $request
     * @return View
     */
    public function postSubscriberAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /* @var $subscriberObject stdClass */
        $subscriberObject = json_decode($request->getContent());
        $domain = $subscriberObject->domain;
        /* @var $converterService ConverterService */
        $converterService = $this->get('converter_service');
        $ipAddress = $converterService->forwardDnsLookup($domain);

        $subscriber = new Subscriber();
        $subscriber->setName($subscriberObject->name);
        $subscriber->setEmail($subscriberObject->email);
        $subscriber->setIpAddress($ipAddress);

        try {
            $em->persist($subscriber);
            $em->flush();
            return $this->view(
                $subscriber,
                200
            );
        } catch (UniqueConstraintViolationException $ue) {
            return $this->view(
                array("message" => "There is already a subscriber with the same email address"),
                409
            );
        }
    }
}