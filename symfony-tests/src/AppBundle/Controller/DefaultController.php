<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $subscriberRepo = $this->getDoctrine()->getRepository('AppBundle:Subscriber');
        $subscribers = $subscriberRepo->findAll();

        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'subscribers' => $subscribers,
        ));
    }

    /**
     * @Route("/subscribers/{id}", name="subscriber_details")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSubscriberAction(Request $request, $id)
    {
        $subscriberRepo = $this->getDoctrine()->getRepository('AppBundle:Subscriber');
        $subscriber = $subscriberRepo->find($id);

        return $this->render('default/subscriber.html.twig', array(
            'subscriber' => $subscriber,
        ));
    }
}
