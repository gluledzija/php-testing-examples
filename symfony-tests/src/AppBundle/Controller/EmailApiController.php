<?php

namespace AppBundle\Controller;

use AppBundle\Service\EmailService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EmailApiController extends FOSRestController
{
    /**
     * @Get("/api/emails/{email}")
     * @param string $email
     * @return View
     */
    public function getEmailAction($email)
    {
        /* @var $emailValidator EmailService */
        $emailValidator = $this->get('email_service');
        $valid = $emailValidator->isEmailValid($email);

        return $this->view(
            array('isValid' => $valid),
            $valid ? 200 : 400
        );
    }

    /**
     * @Post("/api/emails/action/sorted-last")
     * @param Request $request
     * @return View
     */
    public function sortedLastEmailAction(Request $request)
    {
        /* @var $emailService EmailService */
        $emailService = $this->get('email_service');

        /* @var $inputEmails array */
        $inputEmails = json_decode($request->getContent());

        $allValid = true;
        foreach ($inputEmails as $item) {
            if (!$emailService->isEmailValid($item)) {
                $allValid = false;
                break;
            }
        }

        if ($allValid) {
            $sortedEmails = $emailService->sortEmails($inputEmails);

            return $this->view(
                array(
                    'result' => $sortedEmails[count($sortedEmails) - 1]
                ),
                200
            );
        } else {
            return $this->view(
                array("message" => "There are invalid email messages"),
                400
            );
        }
    }

}