Feature: Subscriber details
  In order to see the subscriber details
  As a website visitor
  I need to be able to list the current subscribers list
  And see details for them

Scenario: Follow subscriber details link and get back
  Given Database is set with one subscriber
  And I am on "/app_behat.php/"
  Then I should see "Welcome to test examples for Symfony"
  And I wait for 1 seconds
  And I should see "TestName"
  When I follow "TestName"
  And I wait for 1 seconds
  Then I should get subscriber details page
  Then I should see "TestName"
  And I should see "test@example.com"
  And I should see "111.222.333.444"
  And I wait for 1 seconds
  When I follow "Go Home"
  Then I should see "Welcome to test examples for Symfony"
  And I wait for 1 seconds