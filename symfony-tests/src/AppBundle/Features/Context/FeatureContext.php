<?php

namespace AppBundle\Features\Context;

use AppBundle\Entity\Subscriber;
use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\HttpKernel\KernelInterface;

class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /^Database is set with one subscriber$/
     */
    public function databaseIsSet()
    {
        $this->generateSchema();
        $subscriber = new Subscriber();
        $subscriber->setName('TestName');
        $subscriber->setEmail('test@example.com');
        $subscriber->setIpAddress('111.222.333.444');
        $em = $this->getEntityManager() ;
        $em->persist($subscriber) ;
        $em->flush() ;
    }

    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }

    /**
     * @Then /^I should get subscriber details page$/
     */
    public function iShouldGetSubscriberDetailsPage()
    {
        $this->assertElementContainsText('#subscriber-details-header', 'Subscriber details:');
        // Note: Custom assertion here in order to show usage of Page object
        $page = $this->getMink()->getSession()->getPage();
        $contentHeaderElement = $page->findById('subscriber-details-header');
        $this->assertEquals('Subscriber details:', $contentHeaderElement->getText());
    }

    protected function generateSchema()
    {
        // Get the metadatas of the application to create the schema.
        $metadatas = $this->getMetadatas();

        if ( ! empty($metadatas)) {
            /* @var \Doctrine\ORM\Tools\SchemaTool */
            $tool = new SchemaTool($this->getEntityManager());
            $tool->dropDatabase() ;
            $tool->createSchema($metadatas);
        } else {
            throw new SchemaException('No Metadata Classes to process.');
        }
    }

    /**
     * Overwrite this method to get specific metadatas.
     * @return Array
     */
    protected function getMetadatas()
    {
        $result = $this->getEntityManager()->getMetadataFactory()->getAllMetadata();
        return $result;
    }

    protected function getEntityManager()
    {
        return $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function assertEquals($expected, $actual) {
        if ($expected != $actual) {
            throw new ExpectationException("Actual value '$actual' does not match expected value '$expected'", $this->getMink()->getSession());
        }
    }
}