<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Subscriber;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestFixture implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        // Clear and reset things
        $connection = $this->container->get('doctrine.orm.entity_manager')->getConnection();
        $connection->exec('ALTER SEQUENCE subscriber_id_seq RESTART WITH 1;');

        $subscriber = new Subscriber();
        $subscriber->setName('TestName');
        $subscriber->setEmail('test@example.com');
        $subscriber->setIpAddress('111.222.333.444');

        $manager->persist($subscriber);
        $manager->flush();
    }
}